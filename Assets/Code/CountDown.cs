﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {

    public int countdownTimer;
    public Text countDownDisplay; 
    
    IEnumerable CountDowntoStart()
    {
        while(countdownTimer > 0)
        {
            countDownDisplay.text = countdownTimer.ToString();

            yield return new WaitForSeconds(1f);

            countdownTimer--;
        }

        countDownDisplay.text = "GO!";
        
    }
}
