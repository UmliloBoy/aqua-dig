﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour {

    public Rigidbody2D marineBody;
    public Rigidbody2D player;

    public float speed;
    public float xPositionRight;
    public float yPositionRight;
    public float xPositionLeft;
    public float yPositionLeft;

    public Sprite leftSprite;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (player.GetComponent<SpriteRenderer>().sprite != leftSprite)
            transform.position = new Vector2(marineBody.position.x + xPositionRight, marineBody.position.y + yPositionRight);
        else
            transform.position = new Vector2(marineBody.position.x + xPositionLeft, marineBody.position.y + yPositionLeft);
    }

}
