﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

    public Rigidbody2D player;
    public Rigidbody2D blade;
    public VirtualJoystick movement;
    public bool pauseMenu = true;
    public float timeLeft = 100f;
    public Slider volumeSlider;

    public Sprite leftSprite;
    public Canvas pauseMenuCanvas;
    public Canvas optionsMenuCanvas;
    public Canvas scoreboardMenuCanvas;
    public Canvas helpmenuCanvas;
    public Image ControllerPlay;

    private Sprite originalSprite;

    public float push;
    public float speed;

    private bool allowAllow = true;
    private string hitDirection;
    private float decreaseSize = 0.001f;
    private float pivoteYMove = 0.0005f;
    private float minimumBoxSize = 0.005000563f;
    private float audioPitch = 0.85f;
    public AudioSource audioSource;
    
    private Collision2D globalCol;

    private bool hitComponent = false;
    private bool moveJoy = true;
    private bool eatTile = false;
    private bool stillEating = false;

    public int countdownTimer;
    public float originalPositionControllerX;
    public Text countDownDisplay;
    public Text Health;
    public Text TextScore;
    public Text highScore;
    public Text gameOver;

    // Use this for initialization
    void Start () {

        originalPositionControllerX = ControllerPlay.transform.position.x;

        if (PlayerPrefs.GetString("ControllerPosition") == "LEFT")
        {
            moveControllerLeft();
        }
        else
        {
            moveControllerRight();
        }

        StartCoroutine(CountDown());
        
        originalSprite = this.GetComponent<SpriteRenderer>().sprite;
    }

    IEnumerator CountDown()
    {
        yield return new WaitForSeconds(1f);

        countDownDisplay.gameObject.SetActive(false);
    }

    IEnumerator TimeEndGame()
    {
        yield return new WaitForSeconds(5f);

        Application.Quit();
        
    }
    
    public void volumeChange()
    {
        audioSource.volume = volumeSlider.value;
    }

    public void showScoreBoard()
    {
        pauseMenuCanvas.enabled = false;

        scoreboardMenuCanvas.enabled = true;

        highScore.text = PlayerPrefs.GetInt("HIGHSCORE").ToString();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "crane")
        {
            timeLeft = 100f;
            
            Time.timeScale = 0;
            audioSource.volume = 0.100f;
            audioSource.pitch = audioPitch;

            pauseMenuCanvas.enabled = true;
        }
        else if (col.name == "Air Refresh")
        {
            timeLeft = 100f;
        }
    }

    public void HelpMenu()
    {
        pauseMenuCanvas.enabled = false;
        helpmenuCanvas.enabled = true;
    }

    public void moveControllerRight()
    {
        ControllerPlay.transform.position = new Vector3(originalPositionControllerX, 0, 0);
        PlayerPrefs.SetString("ControllerPosition", "RIGHT");
        PlayerPrefs.Save();
    }

    public void moveControllerLeft()
    {
        ControllerPlay.transform.position = new Vector3(0, 0, 0);
        PlayerPrefs.SetString("ControllerPosition", "LEFT");
        PlayerPrefs.Save();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            Health.text = "" + Mathf.Round(timeLeft);
            if (timeLeft < 0)
            {
                Debug.Log("YOU DIED.");

                Time.timeScale = 0;

                if (int.Parse(TextScore.text) > PlayerPrefs.GetInt("HIGHSCORE"))
                {
                    PlayerPrefs.SetInt("HIGHSCORE", int.Parse(TextScore.text));
                    PlayerPrefs.Save();
                }

                gameOver.text = "GAME OVER!";
                gameOver.color = Color.red;
                gameOver.gameObject.SetActive(true);

                StartCoroutine(TimeEndGame());
            }
        }

        //If the player is allowd to move
        if (moveJoy)
        {
            //This code will enable a force in the players desired direction
            if (movement.InputDirection != Vector2.zero && transform.position.y < 0)
                player.velocity = movement.InputDirection * push;

            // This code changes the facing direction of the player image
            if (movement.InputDirection.x < 0)
            {
                this.GetComponent<SpriteRenderer>().sprite = leftSprite;
                //player.GetComponent<Collider2D>().offset = new Vector2(0.03f, 0);
            }
            else if (movement.InputDirection.x > 0)
            {
                this.GetComponent<SpriteRenderer>().sprite = originalSprite;
                //player.GetComponent<Collider2D>().offset = new Vector2(-0.03f, 0);
            }
        }

        if (transform.position.y > 0)
        {
            movement.OnPointerUp(null);
        }

        //Clear Collider
        if (movement.InputDirection.y > 0.3f)
        {
            hitComponent = false;
        }

        if (eatTile)
        {
            if (globalCol != null)
            {

                //Debug.Log("X Side: " + globalCol.contacts[0].normal.x + "\nY Side: " + globalCol.contacts[0].normal.y);
                globalCol.gameObject.GetComponentInChildren<ParticleSystem>().Play();

                //Eating tille from top to bottom
                if (globalCol.collider.GetComponent<SpriteRenderer>().size.y > 0 && 
                    allowAllow && 
                        globalCol.contacts[0].normal.y > 0.5f &&
                                hitDirection == "Down")
                {

                    player.position = Vector3.MoveTowards(player.position, new Vector3(globalCol.collider.transform.position.x, globalCol.collider.transform.position.y), 1 * Time.deltaTime);
                    globalCol.collider.GetComponent<SpriteRenderer>().drawMode = SpriteDrawMode.Tiled;
                    globalCol.collider.GetComponent<SpriteRenderer>().size = globalCol.collider.GetComponent<SpriteRenderer>().size - new Vector2(0, decreaseSize);
                    globalCol.collider.GetComponent<BoxCollider2D>().size = globalCol.collider.GetComponent<BoxCollider2D>().size - new Vector2(0, decreaseSize);
                    globalCol.collider.GetComponent<RectTransform>().transform.position -= new Vector3(0, pivoteYMove, 0);

                    blade.transform.Rotate(0, 0, speed * Time.deltaTime);
                    moveJoy = false;

                    if (globalCol.collider.GetComponent<SpriteRenderer>().size.y < minimumBoxSize)
                    {
                        allowAllow = false;
                        globalCol.collider.GetComponent<SpriteRenderer>().size = new Vector2(0, 0);
                    }

                    movement.OnPointerUp(null);
                    stillEating = true;
                    
                }
                //Eating tile from left to right
                else if (globalCol.collider.GetComponent<SpriteRenderer>().size.y > 0 &&
                            allowAllow &&
                                globalCol.contacts[0].normal.x < -0.5f &&
                                    hitDirection == "Right")
                {
                    player.position = Vector3.MoveTowards(player.position, new Vector3(globalCol.collider.transform.position.x, globalCol.collider.transform.position.y), 1 * Time.deltaTime);
                    globalCol.collider.GetComponent<SpriteRenderer>().drawMode = SpriteDrawMode.Tiled;
                    globalCol.collider.GetComponent<SpriteRenderer>().size = globalCol.collider.GetComponent<SpriteRenderer>().size - new Vector2(decreaseSize, 0);
                    globalCol.collider.GetComponent<BoxCollider2D>().size = globalCol.collider.GetComponent<BoxCollider2D>().size - new Vector2(decreaseSize, 0);
                    globalCol.collider.GetComponent<RectTransform>().transform.position -= new Vector3(pivoteYMove * -1, 0, 0);

                    blade.transform.Rotate(0, 0, speed * Time.deltaTime);
                    moveJoy = false;

                    if (globalCol.collider.GetComponent<SpriteRenderer>().size.x < 0.005000563f)
                    {
                        allowAllow = false;
                        globalCol.collider.GetComponent<SpriteRenderer>().size = new Vector2(0, 0);
                    }
                    
                    movement.OnPointerUp(null);
                    stillEating = true;
                }
                //Eating tile from right to left
                else if (globalCol.collider.GetComponent<SpriteRenderer>().size.y > 0 &&
                            allowAllow &&
                                globalCol.contacts[0].normal.x > 0.5f &&
                                    hitDirection == "Left")
                {
                    player.position = Vector3.MoveTowards(player.position, new Vector3(globalCol.collider.transform.position.x, globalCol.collider.transform.position.y), 1 * Time.deltaTime);
                    globalCol.collider.GetComponent<SpriteRenderer>().drawMode = SpriteDrawMode.Tiled;
                    globalCol.collider.GetComponent<SpriteRenderer>().size = globalCol.collider.GetComponent<SpriteRenderer>().size - new Vector2(decreaseSize, 0);
                    globalCol.collider.GetComponent<BoxCollider2D>().size = globalCol.collider.GetComponent<BoxCollider2D>().size - new Vector2(decreaseSize, 0);
                    globalCol.collider.GetComponent<RectTransform>().transform.position -= new Vector3(pivoteYMove, 0, 0);

                    blade.transform.Rotate(0, 0, speed * Time.deltaTime);
                    moveJoy = false;

                    if (globalCol.collider.GetComponent<SpriteRenderer>().size.x < minimumBoxSize)

                    {
                        allowAllow = false;
                        globalCol.collider.GetComponent<SpriteRenderer>().size = new Vector2(0, 0);
                    }

                    movement.OnPointerUp(null);
                    stillEating = true;
                }
                //This deletes the final left overs of load and resets components for the next component to be digged.
                else if (stillEating == true)
                {
                    if (true)
                    {
                        globalCol.collider.GetComponent<Collider2D>().isTrigger = true;
                        globalCol.collider.GetComponent<Renderer>().enabled = false;
                        hitComponent = false;
                        blade.transform.Rotate(0, 0, 0);
                        globalCol = null;
                        moveJoy = true;
                        allowAllow = true;
                        eatTile = false;
                        stillEating = false;
                        TextScore.text = (int.Parse(TextScore.text) + 10).ToString();
                    }
                }
            }
        }

        if (hitComponent && movement.InputDirection.y < -0.7 && globalCol.contacts[0].normal.y > 0.5f)   
        {
            eatTile = true;
            hitDirection = "Down";
            Debug.Log("Consume Tile: Down");
        }
        else if (hitComponent && movement.InputDirection.x < -0.7 && globalCol.contacts[0].normal.x > 0.5f)
        {
            eatTile = true;
            hitDirection = "Left";
            Debug.Log("Consume Tile: Left");
        }
        else if (hitComponent && movement.InputDirection.x > 0.7 && globalCol.contacts[0].normal.x < -0.5f)
        {
            eatTile = true;
            hitDirection = "Right";
            Debug.Log("Consume Tile: Right");
        }
    }

    public void options()
    {
        pauseMenuCanvas.enabled = false;
        optionsMenuCanvas.enabled = true;
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        audioSource.pitch = audioPitch;

        pauseMenuCanvas.enabled = true;
    }

    public void ReturnToMain()
    {
        pauseMenuCanvas.enabled = true;
        optionsMenuCanvas.enabled = false;
        scoreboardMenuCanvas.enabled = false;
        helpmenuCanvas.enabled = false;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        audioSource.pitch = 1f;
        pauseMenu = true;

        pauseMenuCanvas.enabled = false;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (!stillEating)
            if (col.collider.tag == "organic_tile")
            {
                Debug.Log(col.collider.name);
                hitComponent = true;
                globalCol = col;
            }
    }
}
