﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    private Image bgImage;
    private Image joystickImage;

    public Vector2 InputDirection { set; get; }
    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos = Vector2.zero;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle
            (bgImage.rectTransform,
                ped.position,
                    ped.pressEventCamera,
                        out pos))
        {
            pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);

             
            float x = (bgImage.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
            float y = (bgImage.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

            InputDirection = new Vector2(x, y);

            InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;


            joystickImage.rectTransform.anchoredPosition =
                new Vector2(InputDirection.x * (bgImage.rectTransform.sizeDelta.x / 3), InputDirection.y * (bgImage.rectTransform.sizeDelta.y / 3));

        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        InputDirection = Vector2.zero;
        joystickImage.rectTransform.anchoredPosition = Vector2.zero;
    }

    // Use this for initialization
    void Start () {

        bgImage = GetComponent<Image>();
        joystickImage = transform.GetChild(0).GetComponentInChildren<Image>();

        InputDirection = Vector2.zero;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
